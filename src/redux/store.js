// React Core

import { createStore } from "redux";
import rootReducer from "./reducer";
import { composeWithDevTools } from "redux-devtools-extension";
const composeEnhances = composeWithDevTools();
const store = createStore(rootReducer, composeEnhances);

export default store;

// //React toolkit
// import { configureStore } from "react-toolkit";
// import filtersReducer from "../components/Filters/FiltersSplice";
// import todoReducer from "../components/TodoList/TodosSplice";

// const store = configureStore({
//   reducer: {
//     filters: filtersReducer,
//     todoList: todoReducer,
//   },
// });

// export default store;
