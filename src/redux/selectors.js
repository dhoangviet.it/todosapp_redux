import { createSelector } from "reselect";

// export const todoListSelector = (state) => {
//   const searchText = searchTextSelector(state);
//   const todosRemaining = state.todoList.filter((todo) => {
//     return todo.name.includes(searchText);
//   });
//   return todosRemaining;
// };
export const todoListSelector = (state) => state.todoList;
export const searchTextSelector = (state) => state.filters.search;
export const filterStatustSelector = (state) => state.filters.status;
export const filterPrioritytSelector = (state) => state.filters.priority;
export const todoRemainingSelector = createSelector(
  todoListSelector,
  filterStatustSelector,
  searchTextSelector,
  filterPrioritytSelector,
  (todo, status, searchText, priority) => {
    // if (priority === []) {
    //   return todo.filter((todo) => todo.name.includes(searchText));
    // }
    return todo.filter((todo) => {
      if (status === "All") {
        return priority.length > 0
          ? todo.name.includes(searchText) && priority.includes(todo.priority)
          : todo.name.includes(searchText);
      }
      return (
        todo.name.includes(searchText) &&
        (status === "Completed" ? todo.completed : !todo.completed) &&
        (priority.length > 0 ? priority.includes(todo.priority) : true)
      );
    });
  }
);
