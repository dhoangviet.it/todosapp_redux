import filtersReducer from "../components/Filters/FiltersSplice";
import todoReducer from "../components/TodoList/TodosSplice";
import { combineReducers } from "redux";
// const rootReducer = (state = {}, action) => {
//   /*
//         {
//             type:'todoList/addTodo',
//             payload:{id:5, name: 'Learn Saga', completed: false, priority: 'Medium'},
//         }
//     */
//   return {
//     filters: filtersReducer(state.filters, action),
//     todoList: todoReducer(state.todoList, action),
//   };
// };
const rootReducer = combineReducers({
  filters: filtersReducer,
  todoList: todoReducer,
});
export default rootReducer;
