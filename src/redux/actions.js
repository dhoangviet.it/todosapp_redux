export const addTodo = (data) => {
  return {
    type: "todoList/addTodo",
    payload: data,
  };
};
export const searchTextChange = (data) => {
  return {
    type: "filters/searchTextChange",
    payload: data,
  };
};
export const filterStatusChange = (data) => {
  return {
    type: "filters/filterStatusChange",
    payload: data,
  };
};
export const filterPriorityChange = (data) => {
  return {
    type: "filters/filterPriorityChange",
    payload: data,
  };
};
export const toggleTodoStatus = (data) => {
  return {
    type: "filters/toggleTodoStatus",
    payload: data,
  };
};
