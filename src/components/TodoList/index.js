import { Col, Row, Input, Button, Select, Tag } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { addTodo } from "../../redux/actions";
import { useState, useRef } from "react";
import { v4 as uuidv4 } from "uuid";
import { todoRemainingSelector } from "../../redux/selectors";
import Todo from "../Todo";

export default function TodoList() {
  const dispatch = useDispatch();
  const inputRef = useRef();
  const [todoName, setTodoName] = useState("");
  const [todoPriority, setTodoPriority] = useState("Medium");
  const todoList = useSelector(todoRemainingSelector);
  // const searchText = useSelector(searchTextSelector);

  // console.log({ searchText });
  const handleAddButtonClick = () => {
    dispatch(
      addTodo({
        id: uuidv4(),
        name: todoName,
        priority: todoPriority,
        completed: false,
      })
    );
    setTodoName("");
    inputRef.current.focus();
    setTodoPriority("Medium");
  };
  const handleChangeInput = (e) => {
    setTodoName(e.target.value);
  };
  const handleChangeSelect = (value) => {
    setTodoPriority(value);
  };
  return (
    <Row style={{ height: "calc(100% - 40px)" }}>
      <Col span={24} style={{ height: "calc(100% - 40px)", overflowY: "auto" }}>
        {/* <Todo name="Learn React" prioriry="High" />
        <Todo name="Learn Redux" prioriry="Medium" />
        <Todo name="Learn JavaScript" prioriry="Low" /> */}
        {todoList.map((data) => (
          <Todo
            key={data.id}
            id={data.id}
            name={data.name}
            prioriry={data.priority}
            completed={data.completed}
          />
        ))}
      </Col>
      <Col span={24}>
        <Input.Group style={{ display: "flex" }} compact>
          <Input ref={inputRef} value={todoName} onChange={handleChangeInput} />
          <Select
            defaultValue="Medium"
            value={todoPriority}
            onChange={handleChangeSelect}
          >
            <Select.Option value="High" label="High">
              <Tag color="red">High</Tag>
            </Select.Option>
            <Select.Option value="Medium" label="Medium">
              <Tag color="blue">Medium</Tag>
            </Select.Option>
            <Select.Option value="Low" label="Low">
              <Tag color="gray">Low</Tag>
            </Select.Option>
          </Select>
          <Button type="primary" onClick={handleAddButtonClick}>
            Add
          </Button>
        </Input.Group>
      </Col>
    </Row>
  );
}
