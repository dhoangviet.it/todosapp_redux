const initState = [
  { id: 1, name: "Learn React", completed: false, priority: "Medium" },
  { id: 2, name: "Learn Redux", completed: false, priority: "Low" },
  { id: 3, name: "Learn Javascript", completed: true, priority: "High" },
];

const todoReducer = (state = initState, action) => {
  /*
          {
              type:'todoList/addTodo',
              payload:{id:5, name: 'Learn Saga', completed: false, priority: 'Medium'},
          }
      */
  switch (action.type) {
    case "todoList/addTodo":
      return [...state, action.payload];
    case "filters/toggleTodoStatus":
      return state.map((todo) =>
        todo.id === action.payload
          ? { ...todo, completed: !todo.completed }
          : todo
      );
    default:
      return state;
  }
};
export default todoReducer;
