const initState = {
  search: "",
  status: "All",
  priority: [],
};

const filtersReducer = (state = initState, action) => {
  /*
          {
              type:'todoList/addTodo',
              payload:{id:5, name: 'Learn Saga', completed: false, priority: 'Medium'},
          }
      */
  switch (action.type) {
    case "filters/searchTextChange":
      return {
        ...state,
        search: action.payload,
      };
    case "filters/filterStatusChange":
      return {
        ...state,
        status: action.payload,
      };
    case "filters/filterPriorityChange":
      return {
        ...state,
        priority: action.payload,
      };
    default:
      return state;
  }
};
export default filtersReducer;
